# Openshift

### Intro
- What is the difference between OpenShift and Kubernetes?
- Find 1 resource that Openshift adds to kubernetes and read about it a little. What feature does Openshift use to extend the kubernetes API (hint: youv'e read about it in the kubernetes book)

### Openshift Installation
- Read about [installing an Openshift cluster](https://docs.openshift.com/container-platform/4.13/installing/index.html#installation-overview_ocp-installation-overview)
- What is a k8s CSI? What is a k8s CNI?

### Cluster Operators
What is a kubernetes operator? in short

Openshift ships with default operators as addons to basic k8s. Read *shortly* about the responsability of the following cluster operators:

- authentication
- console
- etcd
- ingress
- openshift-apiserver & kube-apiserver
- machine-config
- network
- dns
- storage

### Custom Operators
- Hi there, nehfaf. Shamia and Noa here. Worry not, no need to google, just know that installing new custom operators on your cluster is possible and is often the best way to deploy a new service. You can look at examples we use at our environment like group-sync, and external-secrets operator.
- Also, know that writing your own operator is possible.

