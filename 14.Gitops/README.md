# Gitops

### Goals
- The trainee will get familiar with GitOps concept and tools

### Intro
What is the gitops concept? read briefly.

### ArgoCD
- Read about ArgoCD
- Read about ArgoCD's Application then about ApplicationSet resources.

### Helm

- Watch the following [cool video](https://www.youtube.com/watch?v=w51lDVuRWuk)
  <br>No need to watch the "Helm Versions" part
- What templating language is Helm using?
